import shapes3d.*;
import shapes3d.animation.*;
import shapes3d.utils.*;

//Camera Variables
int camX, camY, camZ;
float lookX, lookY, lookZ;
float angle, angleV;

//Used for the moving toroids going between the nuclei
Toroid tor1, tor2, tor3;         //declaring a toroid
Rot rotate, r2, r3;            //declaring a new Rot (Rotation) object
float[] torAngle, ta2, ta3;     //float array for stroing the different angles the toroid will be drawn at
float speed = 0.008f;      //the speed the toroid is moving
float t = 1;               //the toroid position along whatever tube it is on
float s, s2 = 0;               //the toroid position along whatever tube it is on
float pt1, pt2, pt3 = speed;   //temp variables


//tubes used for connecting nuclei
BezTube ctube1, ctube2, ctube3, ctube4, ctube5, ctube6, ctube7, ctube8;
 
//declaring a cell variable
 Cell cell1;
 
//*****************************************************************
void setup() {
  size(700, 700, P3D);
  cell1 = new Cell(this, 0, 0, 0);  //creating a new cell at (0,0,0)
 
 
  //Vectors used for connecting nuclei with tubes:  
    PVector[] a = new PVector[] {
    new PVector(50, 50, 0),
    new PVector(-300, 200, -450),
    new PVector(-1200, 30, -600),
  }; //PVector
  
    PVector[] b = new PVector[] {
    new PVector(50, 50, 0),
    new PVector(250, 200, -450),
    new PVector(400, -500, -850),
  }; //PVector
  
    PVector[] c = new PVector[] {
    new PVector(400, -500, -850),
    new PVector(275, 200, -450),
    new PVector(250, 300, -500),
  }; //PVector
  
    PVector[] d = new PVector[] {
    new PVector(250, 300, -500),
    new PVector(0, 200, -450),
    new PVector(-200, -180, -350),
  }; //PVector
  
    PVector[] e = new PVector[] {
    new PVector(-200, -180, -350),
    new PVector(-500, 100, -450),
    new PVector(-700, 525, -1000),
  }; //PVector
 
    PVector[] f = new PVector[] {
    new PVector(-1200, 30, -600),
    new PVector(-400, -200, -250),
    new PVector(-200, -180, -350),
  }; //PVector
  
    PVector[] g = new PVector[] {
    new PVector(-700, 525, -1000),
    new PVector(-100, -100, -700),
    new PVector(400, -500, -850),
  }; //PVector
  
    PVector[] h = new PVector[] {
    new PVector(-200, -180, -350),
    new PVector(50, 50, 0),
  }; //PVector
  
  
  //Creating all the the connecting tubes (ctube1-ctube8):
  ctube1 = new BezTube(this, new P_Bezier3D(a, a.length), 6, 10, 8);
  ctube1.fill(color(3, 136, 250));
  ctube1.drawMode(Shape3D.SOLID);
  
  ctube2 = new BezTube(this, new P_Bezier3D(b, b.length), 6, 10, 8);
  ctube2.fill(color(3, 136, 250));
  ctube2.drawMode(Shape3D.SOLID);
  
  ctube3 = new BezTube(this, new P_Bezier3D(c, c.length), 6, 10, 8);
  ctube3.fill(color(3, 136, 250));
  ctube3.drawMode(Shape3D.SOLID);
  
  ctube4 = new BezTube(this, new P_Bezier3D(d, d.length), 6, 10, 8);
  ctube4.fill(color(3, 136, 250));
  ctube4.drawMode(Shape3D.SOLID);
  
  ctube5 = new BezTube(this, new P_Bezier3D(e, e.length), 6, 10, 8);
  ctube5.fill(color(3, 136, 250));
  ctube5.drawMode(Shape3D.SOLID);
  
  ctube6 = new BezTube(this, new P_Bezier3D(f, f.length), 6, 10, 8);
  ctube6.fill(color(3, 136, 250));
  ctube6.drawMode(Shape3D.SOLID);
  
  ctube7 = new BezTube(this, new P_Bezier3D(g, g.length), 6, 10, 8);
  ctube7.fill(color(3, 136, 250));
  ctube7.drawMode(Shape3D.SOLID);
  
  ctube8 = new BezTube(this, new P_Bezier3D(h, h.length), 6, 10, 8);
  ctube8.fill(color(3, 136, 250));
  ctube8.drawMode(Shape3D.SOLID);
  
  
  //Creating all of the toroids going from nucleus to nucleus
  tor1 = new Toroid(this, 20, 20);
  tor1.fill(color(250, 120, 20));
  tor1.setRadius(10, 30, 8);
  tor1.drawMode(Shape3D.SOLID);
  
  tor2 = new Toroid(this, 20, 20);
  tor2.fill(color(250, 120, 20));
  tor2.setRadius(10, 30, 8);
  tor2.drawMode(Shape3D.SOLID);
  
  tor3 = new Toroid(this, 20, 20);
  tor3.fill(color(250, 120, 20));
  tor3.setRadius(10, 30, 8);
  tor3.drawMode(Shape3D.SOLID);
  
  
//Camera Variables
  camX = 0;
  camY = 0;
  camZ = 400;
  lookX = 0;
  lookY = 0;
  lookZ = -1000;
  angle = 0;
  angleV = 0;
  
  } //setup
//***************************************************************


void draw() {
   background(0);
   lights();
  
  //Drawing each cell object at a different location
  pushMatrix();
  translate(50, 50, 0);
  cell1.drawCell();
  popMatrix();
  
  pushMatrix();
  translate(-1200, 30, -600);
  cell1.drawCell();
  popMatrix();
  
  pushMatrix();
  translate(400, -500, -850);
  cell1.drawCell();
  popMatrix();
  
  pushMatrix();
  translate(250, 300, -500);
  cell1.drawCell();
  popMatrix();
  
  pushMatrix();
  translate(-200, -180, -350);
  cell1.drawCell();
  popMatrix();
  
  pushMatrix();
  translate(-700, 525, -1000);
  cell1.drawCell();
  popMatrix();
  
//Drawing all of the connecting tubes
 ctube1.draw();
 ctube2.draw();
 ctube3.draw();
 ctube4.draw();
 ctube5.draw();
 ctube6.draw();
 ctube7.draw();
 ctube8.draw();
 
 
 //*******************************************
 //The following code is for drawing the toroids and animating them
 //*******************************************
 
 t += pt1;
 s -= pt2;
 s2 -= pt3;
 
 if (t >= 1) {
   pt1 = -speed;
 } //if
 else if (t <= 0) {
   pt1 = 0;
   t = 0;
   pushMatrix();
   tor1.moveTo(ctube1.getPoint(1));
   popMatrix();
   t+=1;
 } //else if
 
 
 if (s <= 0) {
   pt2 = -speed;
 } //if
  else if (s >= 1) {
   tor2.moveTo(ctube2.getPoint(0));
   s -= 1;
 } //else if
 
 
 if (s2 <= 0) {
   pt3 = -speed;
 } //if
  else if (s2 >= 1) {
   tor3.moveTo(ctube7.getPoint(0));
   s2 -= 1;
 } //else if
 
 
  rotate = new Rot(new PVector(0, 1, 0), ctube1.getTangent(t));
  torAngle = rotate.getAngles(RotOrder.XYZ);
  tor1.moveTo(ctube1.getPoint(t));
  tor1.rotateTo(torAngle);
  tor1.draw();
  
  r2 = new Rot(new PVector(0,1,0), ctube2.getTangent(s));
  ta2 = r2.getAngles(RotOrder.XYZ);
  tor2.moveTo(ctube2.getPoint(s));
  tor2.rotateTo(ta2);
  tor2.draw();
    
  r3 = new Rot(new PVector(0,1,0), ctube7.getTangent(s2));
  ta3 = r3.getAngles(RotOrder.XYZ);
  tor3.moveTo(ctube7.getPoint(s2));
  tor3.rotateTo(ta3);
  tor3.draw();
 
 
//Used for camera
 keyboardInput();
 mousePosition();
 camera(camX, camY, camZ, lookX, lookY, lookZ, 0, 1, 0);
   
} //draw
 

//movement
void keyboardInput() {
  if (keyPressed && (key == CODED)) {
    switch (keyCode) {
      case LEFT:
        camX += 20*sin(radians(angle-90));
        camZ += -20*cos(radians(angle-90));
        break;
        
      case RIGHT:
        camX += -20*sin(radians(angle-90));
        camZ += 20*cos(radians(angle-90));
        break;
        
      case UP:
        camX += 20*sin(radians(angle));
        camZ += -20*cos(radians(angle));
        break;
        
      case DOWN:
        camX += -20*sin(radians(angle));
        camZ += 20*cos(radians(angle));
        break;
        
        
      default:
        break;        
      
    } //switch
  } //if
  
} //keyboardInput


void mousePosition() {
  float mouse = mouseX;
  float mou = mouseY;
  float wid = width;
  float hei = height;
  angle = ((mouse/wid)-0.5)*360;
  angleV = ((mou/hei)-0.5)*180;
  updatube_poseCameraPointube_posing();
} //mousePositube_posion


void updatube_poseCameraPointube_posing() {
  lookX = 100000*sin(radians(angle));
  lookY = 100000*sin(radians(angleV));
  lookZ = -100000*cos(radians(angle));
} //updatube_poseCameraPointube_posing



import shapes3d.*;
import shapes3d.animation.*;
import shapes3d.utils.*;

//***Disregard these camera variables for now*****//
int camX, camY, camZ;
float lookX, lookY, lookZ;
float angle, angleV;

private BezTube mytube;


int segs = 10;
int slices = 8;

void setup() {
  size(500, 500, P3D);
  mytube = createBTube();
  
  
 
  camX = 0;
  camY = 0;
  camZ = -5;
  lookX = 0;
  lookY = 0;
  lookZ = -1000;
  angle = 0;
  angleV = 0;
  
  
  
  } //setup


void draw() {
  background(255);
  camera(0, 0, 250, 0, 0, 0, 0, 1, 0);
 
 pushMatrix();
 translate(50, 50, 0);
 sphere(30);
 fill(0, 255, 0);
 stroke(160, 255, 213);
 popMatrix();
 
 mytube.draw();  //1st tube that goes straight down the middle of the sphere
  
  //Second tube going clockwise from first tube
  pushMatrix();
  translate(30, 20, 0);
  rotateX(radians(-45));
  mytube.draw();
  popMatrix();
  
  //3rd tube going clockwise from first tube
 pushMatrix();
  translate(30, 20, 0);
  rotate(radians(45));
  mytube.draw();
  popMatrix();
  
  //4th tube going clockwise from first tube
  pushMatrix();
  translate(100, 53, 0);
  rotate(radians(120));
  mytube.draw();
  popMatrix();
  
  //1st tube going counter clockwise from first tube
  pushMatrix();
  translate(5, -170, 0);
  rotate(radians(160));
  mytube.draw();
  popMatrix();
  
  
//*****Disregard for now*************//

 keyboardInput();
 mousePosition();
 camera(camX, camY, camZ, lookX, lookY, lookZ, 0, 1, 0);
 
   
} //draw


public BezTube createBTube() {
  PVector[] p = new PVector[] {
    new PVector(50, -200, 0),
    new PVector(70, -70, 0),
    new PVector(50, 30, 0),
   
  }; //PVector
  
  BezTube bt = new BezTube(this, new P_Bezier3D(p, p.length), 2, segs, slices);
  
  bt.fill(color(128, 213, 222));
  bt.stroke(color(0, 0, 0));
  bt.strokeWeight(.5f);
  bt.drawMode(Shape3D.SOLID | BezTube.BOTH_CAP);
  
  return bt;
  
} //createBTube


//movement
void keyboardInput() {
  if (keyPressed && (key == CODED)) {
    switch (keyCode) {
      case LEFT:
        camX += 20*sin(radians(angle-90));
        camZ += -20*cos(radians(angle-90));
        break;
        
      case RIGHT:
        camX += -20*sin(radians(angle-90));
        camZ += 20*cos(radians(angle-90));
        break;
        
      case UP:
        camX += 20*sin(radians(angle));
        camZ += -20*cos(radians(angle));
        break;
        
      case DOWN:
        camX += -20*sin(radians(angle));
        camZ += 20*cos(radians(angle));
        break;
        
        
      default:
        break;        
      
    } //switch
  } //if
  
} //keyboardInput




void mousePosition() {
  float mouse = mouseX;
  float mou = mouseY;
  float wid = width;
  float hei = height;
  angle = ((mouse/wid)-0.5)*360;
  angleV = ((mou/hei)-0.5)*180;
  updateCameraPointing();
} //mousePosition



void updateCameraPointing() {
  lookX = 100000*sin(radians(angle));
  lookY = 100000*sin(radians(angleV));
  lookZ = -100000*cos(radians(angle));
} //updateCameraPointing